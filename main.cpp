#include <iostream>
#include <fstream>
#include <list>
#include <string>

using namespace std;

class listWord {
    public:
        string word;
        int count;
        list<int> lines;

        listWord() {
            word = "";
            count = 0;
        }

        listWord(string str) {
            word = str;
            count = 1;
        }
};

bool equals(string a, string b) {
    return a.compare(b) == 0;
}

string cleanified(string s) {
    string result = s;
    int i = 0;
    while (s[i] != '\0' && i < s.length()) {
        if (s[i] >= 'A' && s[i] <= 'Z'){
            s[i] = s[i] + 32;
            result[i] = s[i];
        } else if (s[i] == '.' || s[i] == ',' || s[i] == '!' || s[i] == '?' || s[i] == ':' || s[i] == ';' || s[i] == '-' || s[i] == '+') {
            result.erase(i);
        }
        ++i;
    }
    return result;
    
}

class wordArray {
    public:
        int length;
        wordArray() {
            length = 0;
            list = new (nothrow) listWord [0];
        }

        int add_word(string str, int line_number, bool sort) {
            if (list == nullptr) {
                return 0;
            }

            string w = cleanified(str);
            if (w == "")
                return 0;

            for (int i = 0; i < length; ++i) {
                if (equals(list[i].word, w)){
                    if (sort) {
                        listWord swap = list[i];
                        ++swap.count;
                        swap.lines.push_back(line_number);
                        int j = i;
                        for (j; j > 0 && (list[j-1].count < swap.count); --j)
                            list[j] = list[j - 1];
                        list[j] = swap;
                    } else {
                        ++list[i].count;
                        list[i].lines.push_back(line_number);
                    }
                    return 1;
                }
            }
            
            listWord* copy = new (nothrow) listWord [length + 1];
            for (int i = 0; i < length; ++i){
                copy[i] = list[i];
            }
            list = copy;
            listWord new_word = listWord(w);
            new_word.lines.push_back(line_number);
            list[length] = new_word;
            ++length;
            return 1;
        }

        bool is_empty() {
            return length == 0;
        }

        listWord get_word(int i) {
            return list[i];
        }

    private:
        listWord* list;
};

list<string> split(string input, string str) {
    list<string> result;
    int pos = input.length() - 1;
    while ((pos = input.find(str)) != string::npos) {
        string s = input.substr(0, pos);
        result.push_back(s);
        input.erase(0, pos + str.length());
    }
    result.push_back(input.substr(0, pos));
    return result;
}

list<string> split_list(list<string> l, string s) {
    list<string> copy = l;
    list<string> result;
    while (!copy.empty()) {
        list<string> a = split(copy.front(), s);
        while (!a.empty()) {
            result.push_back(a.front());
            a.pop_front();
        }
        copy.pop_front();
    }
    return result;
}

list<string> split_words(string s) {
    list<string> result = split(s, " ");
    
    result = split_list(result, ",");
    result = split_list(result, ".");
    result = split_list(result, ";");

    return result;
}


int main (int argc, char** argv) {
    string text_file = "text.txt";
    string result_file = "result.csv";
    string option = "";
    bool sort = false;

    cout << "Sort result by ocurrences?\nEnter 'y' to confirm: ";
    cin >> option;
    if (equals(option, "y"))
        sort = true;
    
    ifstream input(text_file);
    wordArray words;
    if (input.is_open()) {
        cout << "\nCounting words" << (sort ? " (sorted)" : "") << "..." << endl;
        string line;
        int line_no = 1;
        while (getline(input, line)) {
            list<string> parts = split_words(line);
            // cout << "Number of words in '" << line << "': " << parts.size() << endl;
            while (!parts.empty()) {
                words.add_word(parts.front(), line_no, sort);
                parts.pop_front();
            }
            ++line_no;
        }
        input.close();
        cout << "Counting finished." << endl;
    } else
        cout << "Unable to open file.";
    
    ofstream out(result_file);
    int sum = 0;
//    out << "\xef\xbb\xbf";
    out << "Word,Count,Lines" << endl;
    for (int i = 0; i < words.length; ++i) {
        sum += words.get_word(i).count;
        string l = "";
        list<int> ln = words.get_word(i).lines;
        out << words.get_word(i).word << "," << words.get_word(i).count << ",";
        while (!ln.empty()){
            out << ln.front() << ";";
            ln.pop_front();
        }
        out << endl;

    }
    /* out << "Distinct words," << words.length << ",\n";
    out << "Total," << sum << ",\n"; */

    out.close();

    cout << "\nResult written to '" << result_file << "'." << endl;

    
    return 1;
}