## Wordcounter
(c) Hannes Bruger, 2020

http://www.hannes-bruger.de/projects/wordcounter.html

### Execution
- Download Wordcounter.exe
- Write the text you want to count the words of to "text.txt"
- No Installation needed, just execute the program
- The result will be written to "result.csv"